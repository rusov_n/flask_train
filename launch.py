from flask import Flask, render_template
app = Flask(__name__, static_path='/static')  # static_path='/templates'


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<operation>')
def operate(operation):
    return render_template('operation.html', operation=operation)

if __name__ == '__main__':
    app.run()
